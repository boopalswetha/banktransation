package com.hcl.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hcl.model.User;
import com.hcl.repository.UserRepository;


@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	private Random random= new Random();
	
	@Override
	public User createUser(@Valid User user) {

		int leftLimit = 97;
		int rightLimit = 122;
		int targetStringLength = 10;
		
		String generatedString = random.ints(leftLimit, rightLimit + 1).limit(targetStringLength)
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
		user.setPassword(generatedString);
		return userRepository.save(user);
	}

	@Override
	public List<User> getAllUsers() {
		List<User> userList = (List<User>) userRepository.findAll();

		if (userList.size() > 0) {
			return userList;
		} else {
			return new ArrayList<User>();
		}
	}
    
}