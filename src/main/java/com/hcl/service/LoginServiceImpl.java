package com.hcl.service;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hcl.Exception.UserNotfoundException;
import com.hcl.model.User;
import com.hcl.repository.UserRepository;


@Service
public class LoginServiceImpl implements LoginService{

	@Autowired
	UserRepository loginRepository;

	@Override
	public User login(User user) {
		User login = loginRepository.findByCustomerIdAndPassword(user.getCustomerId(), user.getPassword());
	if(login==null) {
		throw new UserNotfoundException("Enter correct Id and Password");
	}
	if(!login.getPassword().equals(user.getPassword())) {
		throw new UserNotfoundException("Password mismatch");
		}
	return login;
}

	
}
