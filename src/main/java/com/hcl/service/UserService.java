package com.hcl.service;

import java.util.List;
import com.hcl.model.User;

public interface UserService {
	
	public List<User> getAllUsers();
	public User createUser(User user);
	
}
