package com.hcl.service;


import com.hcl.model.User;


public interface LoginService {
	User login(User user);
}

