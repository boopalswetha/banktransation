package com.hcl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.model.Trasnsction;
import com.hcl.service.TransacService;

@RestController
public class TransactionController {

	@Autowired
	TransacService transactionservice;

	@PostMapping("/transaction")
	public ResponseEntity<String> transaction(@RequestBody Trasnsction trasnsction) {
		if (transactionservice.transaction(trasnsction)) {
			return new ResponseEntity<>("Transaction is sucessfull,TransactionDetails: " + trasnsction, HttpStatus.OK);
		}

		else {
			return new ResponseEntity<>("Transaction is Failure,TransactionDetails: " + trasnsction,
					HttpStatus.BAD_REQUEST);

		}

	}

}
