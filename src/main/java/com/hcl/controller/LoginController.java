package com.hcl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.model.User;
import com.hcl.service.LoginService;


@RestController
public class LoginController {

	@Autowired
	LoginService loginService;
	

	@PostMapping(value = "/login")
	public ResponseEntity<String> login(@RequestBody User user) {
	    @SuppressWarnings("unused")
		User loginUser = loginService.login(user);
		return new ResponseEntity<String>("User Logged  in successfully", HttpStatus.OK);

	
}
}
