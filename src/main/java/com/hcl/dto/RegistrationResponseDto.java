package com.hcl.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


public class RegistrationResponseDto {

	@Size(min = 3, max = 9, message = "the custometr name is size 3  to 9")
	private String customerName;
	@NotEmpty(message = "AadharNo should not be empty")
	private Long aadharNo;
	@NotEmpty(message = "Occupation Should not be blank")
	private String occupation;
	@NotEmpty(message = "Address Should not be blank")
	private String address;
	private Date DOB;
	private String gender;
	@Size(min = 10, max = 10, message = "the Phone number should have only 10 digits")
	private String phone;
}
